#Models

## Creating & Naming a Model
Please use the `php artisan make:model {name}` command. Please be aware we use model cammel cased names like; `Orders`, `Carts` or `DeliveredOrders`. Most of the time it is the same (camel cased) as the Table name behind it. 

## Location to store. 
We prefer use only the `{PROJECT-ROOT}/app` folder/namespace as Laravel uses by default. Sometimes when projects are bigger we need to sub-folder models to keep overview of the project. **Only then** we using the `{PROJECT-ROOT}/app/Models` folder/namespace. 

## Guidelines
- Only put Models into (sub-)folders when you'll have more than ±10 Models or having needs to generate overview.
- Always defines table names (if DB model) like; `protected $table = '{TABLENAME}';`

**Example**
