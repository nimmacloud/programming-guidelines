# Controllers

## Creating & Naming a Controller
Please use the `php artisan make:controller {name}Controller` command. Please be aware we use controller cammel cased names like; `HomeController`, `DashboardController` or `MyAccountPasswordController`. But **always** use `...Controller` at the end of the name.

## Location to store. 
When you just write a 'simple' application. We prefer your use only the `app/Controllers` folder. When youre Application has more authentication levels like; admin, manager, employee, customer and so one.. Then 99% of the situations you better use `app/Controllers/{level}`. Like;

*Simple App:*  
`app/Controller/HomeController`   

*Larger apps with Auth. Levels App:*      
`app/Controllers/Manager/HomeController`      
`app/Controllers/Employee/EmployeesController`    
`app/Controllers/Customer/CartController`     

## Re-use a Controller as Global for multiple (level) purposes.
**[NOTE!] - Do not use multiple controllers for the same purpose. Like a password-reset of showing a dashboard. Those differences you need to take care of inside your controller.** 

*Wrong:*    
`app/Controllers/Manager/DashboardController`     
`app/Controllers/Employee/DashboardController`    
`app/Controllers/Customer/DashboardController`    

*Valid:*      
`app/Controllers/Global/DashboardController`

## Code layout aproach
Below you'll find a example Controller with the right manner of coding. Watch the placement and usage. 

**Guidelines;**
- Each Class has his own Copyright above the code. `Find a plugin for your IDE to autamticcly add this to new classes.` 
- Controller names are written in Camel Case. 
- All `used` classes are sorted by Laravel -> then your own models.
- Always use a `protected $info` variable on top inside your class. 
- Functions has always an PHP Doc witch provides bit comment and showing variables. `phpdoc ide plugin's` can be found for each IDE.
- *Do not* use `public function index(Request $request)` to call the request instance. Use the helper `request()->...`
- Inside a function we use the `protected $info` variable to fill with data and return the whole array to the view.

**Example;**

```php
<?php
/**
 *
 * Copyright Mainpixel BV (C)
 * Project: {PROJECTNAME}.
 * Last Modified: 8/1/18 8:17 AM
 * Mainpixel B.V. - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Written by {YOUR NAME}
 *
 */

namespace App\Controllers;

use App\Http\Controllers\Controller;
use App\Orders;

class DashboardController extends Controller
{
    protected $info;

    /**
     * This function loads a dashboard.
     *
     * @return void
     */
    public function index()
    {
        $this->info['list'] = (new Orders)->orderBy('updated_at','desc')->paginate(10);
        return view('pages.dashboard',$this->info);
    }
}


```