# Plugins helping you by writing the perfect code for us.

## PHP DocBlocker.
https://github.com/neild3r/vscode-php-docblocker    
By typing `/**` and hit return above a function inside a Controller or Model will give you a Doc block snippet.

## PHP Code Formating.
https://github.com/junstyle/vscode-php-cs-fixer 
This will helps you formating code following this guidelines.

## Laravel Blade spacer.
https://github.com/austenc/vscode-blade-spacer  
Helps you write quickly `{{` and `{!!` tags.

## Auto Close Tags.
https://github.com/formulahendry/vscode-auto-close-tag  
Helping you writing you blades.


